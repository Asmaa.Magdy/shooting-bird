let dialog = document.querySelector('#dialog1');
let dialogDiv = document.querySelector('.dialog1');
let startBtn = document.querySelector('#play');
startBtn.setAttribute('startBtn', 'startFun()');

let closebtn = document.createElement('a');
closebtn.setAttribute('class', 'closebtn');
let closeImg = document.createElement('img');
closeImg.setAttribute('src', '/assets/images/close.png');
closeImg.setAttribute('class', 'closeImg');
closebtn.appendChild(closeImg);

let input = document.createElement('input');
input.setAttribute('type', 'text');
input.setAttribute('id', 'name');

let playBtn = document.createElement('button');
playBtn.innerText = 'Play';
playBtn.setAttribute('class', 'play');

dialogDiv.appendChild(closebtn);
dialogDiv.appendChild(input);
dialogDiv.appendChild(playBtn);

dialog.appendChild(dialogDiv);

startBtn.addEventListener('click', () => {
    dialog.showModal();
});

closebtn.addEventListener('click', () => {
    dialog.close();
});


export { playBtn, dialog };
