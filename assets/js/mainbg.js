import { birds, ssType } from './birds.js';
let dialog = document.querySelector('#dialog3');
let d = document.querySelector('.dialog3');
let score;
let lives;
let sText;
let t = 0;
let sType;

sText = document.createElement('p');
d.appendChild(sText);
function changebg() {
    let btn = document.querySelector('#play');
    document.body.style.backgroundImage =
        "url('/assets/images/gamebackGround.jpg')";
    document.body.style.backgroundRepeat = 'repeat';
    document.body.style.backgroundSize = '100% 650px';
    btn.style.display = 'none';
    score = document.createElement('p');
    score.setAttribute('class', 'score');
    score.textContent = 'Score';
    lives = document.createElement('p');
    lives.setAttribute('class', 'lives');

    lives.textContent = '0';
    document.body.appendChild(score);
    birds();
    document.body.appendChild(lives);

    sType = setInterval(timer, 1000);
}

function timer() {
    lives.textContent = '';
    if (t === 20) {
        sText.textContent = 'Your ' + score.textContent;
        score.innerText = 'Score';
        dialog.showModal();
        clearInterval(sType);
        clearInterval(ssType);
        t = 0;
        
        
    } else {
        t += 1;
        lives.textContent = t;
    }
}

export { changebg, score, lives };
